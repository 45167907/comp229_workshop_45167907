﻿# Task 0

Clone this repository (well done!)

# Task 1

Take a look at the two repositories:

  * (A) https://bitbucket.org/altmattr/personalised-correlation/src/master/
  * (B) https://github.com/Whiley/WhileyCompiler

And answer the following questions about them:

  * These repositories are at two different websites - github and bitbucket - what are these sites?  What service do they provide? Which is better?
	The first is bitbucket, the second is GitHub. Each is a version control website that allows users to organise their work that which is based off a computer, whether that is coding or the like. Whether one is better than the other is subjective to the person.
  * Who made the last commit to repository A?
	Tam Du
  * Who made the first commit to repository A?
	Jon Mountjoy
  * Who made the first and last commits to repository B?
	First: DavePearce
	Last: Unable to find due to sheer volume
  * Are either/both of these projects active at the moment?  🤔 If not, what do you think happened?
	They both seem to be active, within the last 7 days.
  * 🤔 Which file in each project has had the most activity?
	Unsure of how to find this.

# Task 2

Setup a new IntelliJ project with a main method that will print the following message to the console when run:

~~~~~
Sheep and Wolves
~~~~~